import Request from './index.js';
import stream from 'stream';
//import { jest } from '@jest/globals';

test('Convert request to url', ()=>{
	expect(Request.url({ path: '/home' })).toEqual('/home');
	expect(Request.url({ path: '/home', method: 'OPTIONS' })).toEqual('/home!');
});

test('Stream to search params', async ()=>{
	const req = stream.Readable.from(['a=b&c=d']);
	req.url = '/';

	const r = new Request(req);

	const p = await r.getData();
	expect(p).toBeInstanceOf(URLSearchParams);
	expect(p.toString()).toBe('a=b&c=d');
});

test('Helper to create a delete request', () => {
	const r = new Request({ url: '/', method: 'GET' });

	expect(Request.url(r.delete('/a'))).toEqual('/a!!');
	expect(Request.url(r.delete())).toEqual('/!!');
});

test('Map GET! request to OPTIONS', () => {
	const request = {
		url: '/resource!',
		method: 'GET'
	};

	const mapped = new Request(request);

	expect(mapped.method).toBe('OPTIONS');
	expect(mapped.path).toBe('/resource');
	expect(mapped.url).toBe('/resource');

	request.method = 'POST';

	const mapped2 = new Request(request);

	expect(mapped2.method).toBe('POST');
	expect(mapped2.path).toBe('/resource');
});

test('Map POST!! request to DELETE', ()=>{
	const request = {
		url: '/resource!!',
		method: 'POST'
	};

	const mapped = new Request(request);

	expect(mapped.method).toBe('DELETE');
	expect(mapped.path).toBe('/resource');

	request.method = 'GET';

	const mapped2 = new Request(request);

	expect(mapped2.method).toBe('GET');
	expect(mapped2.path).toBe('/resource');
});
