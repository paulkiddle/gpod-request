/**
 * Get/Post/Options/Delete Request mapper
 * @module gpod-request
 * @see {@link module:new-lib.default}
 */

export default class Request {
	#req

	/**
	 * Turn the request back to a GET/POST url
	 * @param {Request} request
	 * @returns string
	 */
	static url(request){
		let path = request.path;

		if(request.method === 'DELETE') {
			path += '!!';
		} else if(request.method === 'OPTIONS') {
			path += '!';
		}

		const query = request.query || '';

		return path + query;
	}

	/**
	 * Map a Node request object to a generic GPOD request
	 * @param {HTTP.IncomingRequest} req The Node request object
	 */
	constructor(req) {
		const { pathname, searchParams } = new URL(req.url, 'file://');

		this.path = pathname;
		this.query = searchParams;
		this.method = req.method;
		this.#req = req;

		if(this.path.endsWith('!!')) {
			if(this.method === 'POST') {
				this.method = 'DELETE';
			}
			this.path = this.path.slice(0, -2);
		}

		if(this.path.endsWith('!')) {
			if(this.method === 'GET') {
				this.method = 'OPTIONS';
			}
			this.path = this.path.slice(0, -1);
		}
	}

	/**
	 * Return the path and query part of this object combined
	 */
	get url(){
		return this.path + this.query;
	}

	/**
	 * Read the data body from the request
	 */
	async * [Symbol.asyncIterator](){
		yield* this.#req;
	}

	/**
	 * Return the body as a string
	 * @returns {string}
	 */
	async getBody(){
		let body = '';
		for await(const chunk of this) {
			body+=chunk;
		}
		return body;
	}

	/**
	 * Return the body as a SearchParams object
	 * @returns {URLSearchParams}
	 */
	async getData(){
		return new URLSearchParams(await this.getBody());
	}

	delete(path){
		return new Request({
			url: path || this.url,
			method: 'DELETE'
		});
	}
}
