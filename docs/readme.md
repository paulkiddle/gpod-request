# Get/Post/Options/Delete Request Mapper

Wouldn't it be nice to be able to use OPTIONS or DELETE methods in the browser?

This library lets you do this by treating URLs ending in the `!` character as special cases.

 - `GET /resource!` becomes `OPTIONS /resource`, good for showing an edit form
 - `POST /resource!!` becomes `DELETE /resource`, good for deleting a resource

In all other cases, `!!` and `!` get stripped from your path.
