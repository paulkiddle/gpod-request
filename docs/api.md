<a name="module_gpod-request"></a>

## gpod-request
Get/Post/Options/Delete Request mapper

**See**: [module:new-lib.default](module:new-lib.default)  

* [gpod-request](#module_gpod-request)
    * [module.exports](#exp_module_gpod-request--module.exports) ⏏
        * [new module.exports(req)](#new_module_gpod-request--module.exports_new)
        * _instance_
            * [.url](#module_gpod-request--module.exports+url)
            * [.getBody()](#module_gpod-request--module.exports+getBody) ⇒ <code>string</code>
            * [.getData()](#module_gpod-request--module.exports+getData) ⇒ <code>URLSearchParams</code>
        * _static_
            * [.url(request)](#module_gpod-request--module.exports.url) ⇒

<a name="exp_module_gpod-request--module.exports"></a>

### module.exports ⏏
**Kind**: Exported class  
<a name="new_module_gpod-request--module.exports_new"></a>

#### new module.exports(req)
Map a Node request object to a generic GPOD request


| Param | Type | Description |
| --- | --- | --- |
| req | <code>HTTP.IncomingRequest</code> | The Node request object |

<a name="module_gpod-request--module.exports+url"></a>

#### module.exports.url
Return the path and query part of this object combined

**Kind**: instance property of [<code>module.exports</code>](#exp_module_gpod-request--module.exports)  
<a name="module_gpod-request--module.exports+getBody"></a>

#### module.exports.getBody() ⇒ <code>string</code>
Return the body as a string

**Kind**: instance method of [<code>module.exports</code>](#exp_module_gpod-request--module.exports)  
<a name="module_gpod-request--module.exports+getData"></a>

#### module.exports.getData() ⇒ <code>URLSearchParams</code>
Return the body as a SearchParams object

**Kind**: instance method of [<code>module.exports</code>](#exp_module_gpod-request--module.exports)  
<a name="module_gpod-request--module.exports.url"></a>

#### module.exports.url(request) ⇒
Turn the request back to a GET/POST url

**Kind**: static method of [<code>module.exports</code>](#exp_module_gpod-request--module.exports)  
**Returns**: string  

| Param | Type |
| --- | --- |
| request | <code>Request</code> | 

